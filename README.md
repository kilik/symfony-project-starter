# Symfony Project Starter

Start a new awesome Symfony based project from scratch, we those requirements:

- [docker](doc/docker.md) 
- [docker-compose](doc/docker-compose.md)
- [a reverse proxy](doc/install-a-reverse-proxy-with-docker.md)

## Create my first project from scratch

```bash
git clone https://gitlab.com/kilik/symfony-project-starter.git
cd symfony-project-starter
./create.sh 
```
