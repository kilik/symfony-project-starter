# Install docker

You can follow this official guide to install docker on debian:

https://docs.docker.com/install/linux/docker-ce/debian/

Here is the procedure to install docker on debian 9.x

```bash
sudo apt-get update

sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"

sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io

```

*You should also add your user in the docker group.*

Then, test docker is running:

```bash
docker run hello-world
```
