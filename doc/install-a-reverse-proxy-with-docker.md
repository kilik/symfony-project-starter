# Install a reverse proxy with docker

A reverse proxy you could install in 2 minutes: https://github.com/jwilder/nginx-proxy

```bash
cd ~
git clone https://github.com/jwilder/nginx-proxy
cd nginx-proxy
docker-compose up -d
```

After that, all running docker containers with an environment variable VIRTUAL_HOST set should receive http queries.

