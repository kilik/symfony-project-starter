# Install docker-compose

If you don't want a ten years old docker-compose on debian system, you should install docker-compose manualy. 

Here, the official procedure:

https://docs.docker.com/compose/install/

It's pretty simple (2 commands): 

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

And check the installed version:

```bash
docker-compose --version
```

