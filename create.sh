#!/bin/bash

PHPSTORM_PROJECTS=${HOME}/PhpstormProjects
SYMFONY_VERSION=v4.1.10.1

selectProjectName ()
{
    echo
    echo "What name for your project ?: "
    echo
    echo -n "Project name: "

    read PROJECT_NAME

    if [[ $PROJECT_NAME == "" ]]; then
       echo "project name should not be empty";
       selectProjectName
    fi

    PROJECT_PATH=${PHPSTORM_PROJECTS}/${PROJECT_NAME}

    if [[ -d $PROJECT_PATH ]]; then
       echo "this project already exists";
       selectProjectName
    fi
}

selectProjectType ()
{
    echo
    echo "What kind of project are you starting ?: "
    echo
    echo "1 : web application"
    echo "2 : micro service / minimal application"
    echo
    echo -n "Your choice: "

    read projectType
        case $projectType in
        1)
            echo "Web application"
            PROJECT_TYPE=symfony/website-skeleton
            ;;

        2)
            echo "Micro service / minimal application"
            PROJECT_TYPE=symfony/skeleton

            ;;
        *)
            echo "unknown choice"
            selectProjectType
            ;;
        esac
}

echo "Create a new symfony project with minimal requirements"

# select a project name
selectProjectName

# select a project
selectProjectType

echo "Pulling latest composer image"
docker pull composer

echo "And creating new ${PROJECT_TYPE} project named ${PROJECT_NAME}"

docker run -it --rm  -u ${UID}:${GID} -w /app --volume ${PHPSTORM_PROJECTS}:/app composer create-project ${PROJECT_TYPE} ${PROJECT_NAME} ${SYMFONY_VERSION}

echo "Adding docker files from start pack"
cp dist/* dist/.dockerignore dist/.env.dist ${PROJECT_PATH}/

echo "******************************************"
echo
echo "Your project is ready to start, jump into:"
echo
echo "cd ${PROJECT_PATH}/"
echo
echo "edit your .env.dist, and replace the line"
echo
echo "VHOST=myapp.localhost"
echo
echo "with the domain you want to use"
echo
echo "Then, launch your app:"
echo
echo "make upgrade"
echo
echo "And open your browser: http://myapp.localhost"
echo "for phpmyadmin: http://pma.myapp.localhost"
echo
